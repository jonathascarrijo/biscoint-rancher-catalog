version: '2'
services:
  {{- if eq .Values.BISCOINT_ENVIRONMENT "STAGING" }}
  mongo:
    image: mongo:3.4-jessie
    stdin_open: true
    tty: true
    ports:
    - 27017:27017/tcp
    labels:
      io.rancher.container.pull_image: always
  {{- end }}
  biscoint-meteor:
    {{- if eq .Values.BISCOINT_ENVIRONMENT "PRODUCTION" }}
    image: jonathascarrijo/biscoint-meteor:stable
    {{- end }}
    {{- if eq .Values.BISCOINT_ENVIRONMENT "STAGING" }}
    image: jonathascarrijo/biscoint-meteor:latest
    {{- end }}
    environment:
      {{- if eq .Values.BISCOINT_ENVIRONMENT "PRODUCTION" }}
      MONGO_URL: mongodb://biscoint-mongo:27017/biscoint
      ROOT_URL: https://biscoint.io
      {{- end }}
      {{- if eq .Values.BISCOINT_ENVIRONMENT "STAGING" }}
      MONGO_URL: mongodb://mongo:27017/biscoint
      ROOT_URL: http://staging.biscoint.com.br:3000
      {{- end }}
      METEOR_SETTINGS: '{ "server_only_setting": "foo", "public": { "DEBUG": "false",
        "PRICE_API_REST_BASE_URL": "https://biscoint.io" } }'
    stdin_open: true
	{{- if eq .Values.BISCOINT_ENVIRONMENT "PRODUCTION" }}
    external_links:
    - Biscoint-Mongo/biscoint-mongo:mongo
	{{- end }}
	{{- if eq .Values.BISCOINT_ENVIRONMENT "STAGING" }}
    links:
    - mongo:mongo
	{{- end }}
    tty: true
    ports:
    - 3000:3000/tcp
    labels:
      io.rancher.container.pull_image: always
      io.rancher.scheduler.affinity:host_label: meteor=true
      io.rancher.scheduler.affinity:container_label_ne: io.rancher.stack_service.name=biscoint-meteor
  biscoint-lb:
    image: rancher/lb-service-haproxy:v0.7.15
    ports:
    - 8080:8080/tcp
    labels:
      io.rancher.scheduler.affinity:host_label: load_balancer=true
      io.rancher.container.agent.role: environmentAdmin,agent
      io.rancher.container.agent_service.drain_provider: 'true'
      io.rancher.container.create_agent: 'true'
